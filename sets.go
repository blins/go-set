// Copyright 2018 blins.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package sets


//go:generate ./gentmpl.sh set uint64
//go:generate ./gentmpl.sh set int64
//go:generate ./gentmpl.sh set uint
//go:generate ./gentmpl.sh set int
//go:generate ./gentmpl.sh set byte

